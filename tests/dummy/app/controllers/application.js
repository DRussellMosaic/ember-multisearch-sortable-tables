import Controller from '@ember/controller';
import MultisearchFilterMixin from 'ember-multisearch-sortable-tables/mixins/multisearch-filter-mixin';
import SortableTableColumnMixin from 'ember-multisearch-sortable-tables/mixins/sortable-table-column-mixin';
import { A } from '@ember/array';
import EmberObject from '@ember/object';

export default Controller.extend(MultisearchFilterMixin,SortableTableColumnMixin,{
    filterObjectsTarget: 'application',
    searchFilters: null,
    sortBy: null,
    queryParams: ['date'],

  init() {
    this._super(...arguments);

    var filters = [
      {
        searchTarget: 'firstName',
        groupTarget: 'firstName',
        searchPlaceHolder: 'First Name',
        checked: false, 
      },
      {
        searchTarget: 'disabled',
        groupTarget: 'disabled',
        searchPlaceHolder: 'Disabled',
        type: 'checkbox',
        columnSize: 3,
        checkboxValue: true,
        checked: false
      },

      {
        queryParam: 'date',
        searchPlaceHolder: 'Submitted (After)',
        searchTarget: 'dates',
        groupTarget: 'dates',
        searchPlaceHolder: 'Disabled',
        operation: '<',
        type: 'date',
        checked: true,
        rawValue: false,
        dateValue: null,
        isHasMany: true,
        optionValuePath: "date"
      }, 

      {
        searchTarget: 'ages',
        groupTarget: 'ages',
        searchPlaceHolder: 'Ages',
        checked: true,
        mutate: true,
        associateArrays: ["relatives"]

      },

      {
        searchTarget: 'relatives',
        groupTarget: 'relatives',
        searchPlaceHolder: 'Relatives',
        checked: true,
        mutate: true,
        associateArrays: ["ages"]
      },
      
    ];


    let fullNameGroup = new EmberObject();
    let enrollmentGroup = new EmberObject();

    fullNameGroup.set("label", "USER INFO");
    enrollmentGroup.set("label", "COURSE");

    enrollmentGroup.set("tooltip", 'Selecting filter criteria for courses will trim the list of users displayed below to only those that have enrollments in the course(s) you enter in the filter. On the next step of the process, “Select Courses”, you will still see all the available courses for each student in the list, not just the courses you have filtered the list down to. You will be able to select the courses you want to manage for the selected students from that list on the next step.');

    fullNameGroup.set("grouping", ["firstName", "ages","relatives"]);
    enrollmentGroup.set("grouping", ["date", "disabled"]);

    let filterGrouping = [fullNameGroup, enrollmentGroup];

    let associateArrays = ["ages", "relatives"];

    this.set("filterGrouping", filterGrouping);

    this.set("associateArrays", associateArrays);

    this.setSearchFilters(filters);
    this.set('sortBy', A(['name']));

  }

});
